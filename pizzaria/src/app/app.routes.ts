import { Routes, CanActivate, RouterModule } from '@angular/router';
import { FormComponent } from '../app//form/form.component';
import { ListaComponent } from './lista/lista.component';

export const appRoutes : Routes = [
    { path: 'pizza', component: FormComponent },
    { path: 'lista-pizza', component: ListaComponent}
];

export const AppRoutes = RouterModule.forRoot(appRoutes, { useHash: true });