import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ApiService {

  private API_URL = "http://172.18.0.62:3000";

  constructor(private http: HttpClient) { }

  public getPizza() {
    return this.http.get(`${this.API_URL}/Pizzas`);
  }

  public inseriPizza(data) {
    return this.http.post(`${this.API_URL}/Pizzas`, data);
  }

  public removerPizza(pizzaId) {
    return this.http.delete(`${this.API_URL}/Pizzas/${pizzaId}`);
  }
}
